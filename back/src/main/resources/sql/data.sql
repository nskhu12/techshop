-- Insert categories
INSERT INTO ProductCategory (ProductCategoryName) VALUES ('Laptops');
INSERT INTO ProductCategory (ProductCategoryName) VALUES ('Smartphones');
INSERT INTO ProductCategory (ProductCategoryName) VALUES ('Tablets');
INSERT INTO ProductCategory (ProductCategoryName) VALUES ('Cameras');
INSERT INTO ProductCategory (ProductCategoryName) VALUES ('Audio Devices');

-- Insert Products
-- Insert Products for Category 1
INSERT INTO Product (ProductName, Description, ImageUrl, Price, Category, Manufacturer, StockQuantity, ProductCategoryId, Active, DateCreated, LastUpdated)
VALUES ('High-Performance Laptop', 'Powerful laptop for gaming and productivity', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSqfN9YmVBflhTBil2b_u4eA3yorSPMPXmZujxbh4US-Cz4y7mUogy6oR7jLrQFDBa2QhQ&usqp=CAU', 1299.99, 'Laptops', 'Tech Innovators', 50, 1, true, CURRENT_DATE, CURRENT_DATE);

INSERT INTO Product (ProductName, Description, ImageUrl, Price, Category, Manufacturer, StockQuantity, ProductCategoryId, Active, DateCreated, LastUpdated)
VALUES ('Ultra-Slim Laptop', 'Thin and lightweight laptop for on-the-go use', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRLmtSo_YvfaNkoYTUvDNrTMyqUeo8pYF80kg&usqp=CAU', 899.99, 'Laptops', 'Slim Tech', 30, 1, true, CURRENT_DATE, CURRENT_DATE);

INSERT INTO Product (ProductName, Description, ImageUrl, Price, Category, Manufacturer, StockQuantity, ProductCategoryId, Active, DateCreated, LastUpdated)
VALUES ('Gaming Laptop', 'Specialized laptop for gaming enthusiasts', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMwfpGfS0knB3d9BikykO7ucGbFFZkQ1GG8g&usqp=CAU', 1499.99, 'Laptops', 'Gamer Gear', 20, 1, true, CURRENT_DATE, CURRENT_DATE);

INSERT INTO Product (ProductName, Description, SKU, ImageUrl, Price, Category, Manufacturer, StockQuantity, Active, DateCreated, LastUpdated, ProductCategoryId)
VALUES ('High-Performance Laptop', 'Powerful laptop for gaming and productivity', 'HPL001', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVMvf0oR1ethKzmPNpW1NDasms-LYgbhuEzCqWEdEeEMtThIYf0B9J6snUtyCtONq2aM8&usqp=CAU', 1299.99, 'Laptops', 'Tech Innovators', 50, true, CURRENT_DATE, CURRENT_DATE, 1);

INSERT INTO Product (ProductName, Description, SKU, ImageUrl, Price, Category, Manufacturer, StockQuantity, Active, DateCreated, LastUpdated, ProductCategoryId)
VALUES ('Ultra-Slim Laptop', 'Thin and lightweight laptop for on-the-go use', 'USL001', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTo-dSXGisiHYCquxAoPx9QKLZ97nxfobHviXAxl-0k0Pu320UwQkoM6S9AhzoEuRmbgNA&usqp=CAU', 899.99, 'Laptops', 'Slim Tech', 30, true, CURRENT_DATE, CURRENT_DATE, 1);

INSERT INTO Product (ProductName, Description, SKU, ImageUrl, Price, Category, Manufacturer, StockQuantity, Active, DateCreated, LastUpdated, ProductCategoryId)
VALUES ('Gaming Laptop', 'Specialized laptop for gaming enthusiasts', 'GL001', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSqfN9YmVBflhTBil2b_u4eA3yorSPMPXmZujxbh4US-Cz4y7mUogy6oR7jLrQFDBa2QhQ&usqp=CAU', 1499.99, 'Laptops', 'Gamer Gear', 20, true, CURRENT_DATE, CURRENT_DATE, 1);

-- Insert Products for Category 2
INSERT INTO Product (ProductName, Description, ImageUrl, Price, Category, Manufacturer, StockQuantity, ProductCategoryId, Active, DateCreated, LastUpdated)
VALUES ('Flagship Smartphone', 'The latest flagship smartphone with advanced features', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRtadJYfGQYhQ0BpOqOl-stGWQsX1eGxQ29_g&usqp=CAU', 899.99, 'Smartphones', 'Global Tech', 100, 2, true, CURRENT_DATE, CURRENT_DATE);

INSERT INTO Product (ProductName, Description, ImageUrl, Price, Category, Manufacturer, StockQuantity, ProductCategoryId, Active, DateCreated, LastUpdated)
VALUES ('Camera-Centric Smartphone', 'Smartphone with a focus on photography features', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShN1Io-E5p5iVpNskIr50VHiFcLC5cCz7IQw&usqp=CAU', 749.99, 'Smartphones', 'Photo Tech', 80, 2, true, CURRENT_DATE, CURRENT_DATE);

INSERT INTO Product (ProductName, Description, ImageUrl, Price, Category, Manufacturer, StockQuantity, ProductCategoryId, Active, DateCreated, LastUpdated)
VALUES ('Budget-Friendly Smartphone', 'Affordable smartphone with essential features', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzjkL2yLqdFNpg1hAtZaT8VAlINPtOPDyMKQ&usqp=CAU', 299.99, 'Smartphones', 'Tech Value', 120, 2, true, CURRENT_DATE, CURRENT_DATE);

-- Insert Products for Category 3
INSERT INTO Product (ProductName, Description, SKU, ImageUrl, Price, Category, Manufacturer, StockQuantity, Active, DateCreated, LastUpdated, ProductCategoryId)
VALUES ('Wireless Earbuds', 'High-quality wireless earbuds with noise cancellation', 'WEB001', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRn70GnoMIp66G-hhCUXxeLH-hbFZlnAQnDihMAh7J8rzcYVbXocqWtBRASznX3_NPtLPU&usqp=CAU', 99.99, 'Audio', 'SoundTech', 100, true, CURRENT_DATE, CURRENT_DATE, 3);

INSERT INTO Product (ProductName, Description, SKU, ImageUrl, Price, Category, Manufacturer, StockQuantity, Active, DateCreated, LastUpdated, ProductCategoryId)
VALUES ('Smart Speaker', 'Voice-controlled smart speaker for smart home integration', 'SS001', 'https://iplus.com.ge/images/detailed/6/jbl_jblgo3blkam_go_3_portable_bluetooth_1583006.jpeg', 149.99, 'Audio', 'HomeTech', 50, true, CURRENT_DATE, CURRENT_DATE, 3);

-- Insert Product for Category 4
INSERT INTO Product (ProductName, Description, SKU, ImageUrl, Price, Category, Manufacturer, StockQuantity, Active, DateCreated, LastUpdated, ProductCategoryId)
VALUES ('Fitness Tracker', 'Track your fitness and health activities with this advanced tracker', 'FT001', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRiEePpj2iwZO_mRsrHIFLaF2EwuQcYgeDBsfxAJEVTCtcgDF2rtTpfFoW5XI2qFEvS-ik&usqp=CAU', 79.99, 'Wearable Tech', 'FitTech', 30, true, CURRENT_DATE, CURRENT_DATE, 4);

-- Insert Product for Category 5
INSERT INTO Product (ProductName, Description, SKU, ImageUrl, Price, Category, Manufacturer, StockQuantity, Active, DateCreated, LastUpdated, ProductCategoryId)
VALUES ('Drones with Camera', 'Capture stunning aerial photos and videos with this camera-equipped drone', 'DR001', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsq-qe50cLTPhu0O71Dz61V_r4fwueofIuMxQPRZ_nUR_bbttQzH8cMK5dKE_asI_B3Gc&usqp=CAU', 299.99, 'Gadgets', 'SkyCam', 15, true, CURRENT_DATE, CURRENT_DATE, 5);
