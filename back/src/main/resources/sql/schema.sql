-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mydb` ;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
-- Schema techshop
-- -----------------------------------------------------
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`User` (
  `UserId` BIGINT NOT NULL AUTO_INCREMENT,
  `FirstName` VARCHAR(45) NOT NULL,
  `LastName` VARCHAR(45) NOT NULL,
  `UserName` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(45) NULL,
  `Password` VARCHAR(45) NULL,
  `UserType` VARCHAR(45) NULL,
  PRIMARY KEY (`UserId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`ProductCategory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`ProductCategory` (
  `ProductCategoryId` BIGINT NOT NULL AUTO_INCREMENT,
  `ProductCategoryName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ProductCategoryId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Product` (
  `ProductId` BIGINT NOT NULL AUTO_INCREMENT,
  `ProductName` VARCHAR(45) NOT NULL,
  `SKU` VARCHAR(45) NULL,
  `Description` VARCHAR(200) NULL,
  `ImageUrl` VARCHAR(500) NULL,
  `Price` DECIMAL NOT NULL,
  `Category` VARCHAR(45) NOT NULL,
  `Manufacturer` VARCHAR(45) NOT NULL,
  `StockQuantity` INT NOT NULL,
  `Active` BOOLEAN NOT NULL,
  `DateCreated` DATE,
  `LastUpdated` DATE,
  `ProductCategoryId` BIGINT NOT NULL,
  PRIMARY KEY (`ProductId`),
  INDEX `fk_Product_ProductCategory1_idx` (`ProductCategoryId` ASC) VISIBLE,
  CONSTRAINT `fk_Product_ProductCategory1`
    FOREIGN KEY (`ProductCategoryId`)
    REFERENCES `mydb`.`ProductCategory` (`ProductCategoryId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Review`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Review` (
  `ReviewId` BIGINT NOT NULL AUTO_INCREMENT,
  `ReviewText` VARCHAR(200) NULL,
  `Rating` INT NOT NULL,
  `ImageUrls` VARCHAR(500) NULL,
  `UserId` BIGINT NOT NULL,
  `ProductId` BIGINT NOT NULL,
  PRIMARY KEY (`ReviewId`),
  INDEX `fk_Review_User1_idx` (`UserId` ASC) VISIBLE,
  INDEX `fk_Review_Product1_idx` (`ProductId` ASC) VISIBLE,
  CONSTRAINT `fk_Review_User1`
    FOREIGN KEY (`UserId`)
    REFERENCES `mydb`.`User` (`UserId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Review_Product1`
    FOREIGN KEY (`ProductId`)
    REFERENCES `mydb`.`Product` (`ProductId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`PaymentMethod`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`PaymentMethod` (
  `PaymentMethodID` BIGINT NOT NULL AUTO_INCREMENT,
  `PaymentType` VARCHAR(45) NOT NULL,
  `AccountDetails` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`PaymentMethodID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Order` (
  `OrderID` BIGINT NOT NULL AUTO_INCREMENT,
  `OrderDate` DATETIME NOT NULL,
  `TotalAmount` INT NOT NULL,
  `OrderStatus` VARCHAR(45) NOT NULL,
  `UserId` BIGINT NOT NULL,
  `PaymentMethod_PaymentMethodID` BIGINT NOT NULL,
  PRIMARY KEY (`OrderID`),
  INDEX `fk_Order_User1_idx` (`UserId` ASC) VISIBLE,
  INDEX `fk_Order_PaymentMethod1_idx` (`PaymentMethod_PaymentMethodID` ASC) VISIBLE,
  CONSTRAINT `fk_Order_User1`
    FOREIGN KEY (`UserId`)
    REFERENCES `mydb`.`User` (`UserId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Order_PaymentMethod1`
    FOREIGN KEY (`PaymentMethod_PaymentMethodID`)
    REFERENCES `mydb`.`PaymentMethod` (`PaymentMethodID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`ShoppingCart`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`ShoppingCart` (
  `ShoppingCartID` BIGINT NOT NULL AUTO_INCREMENT,
  `CreationDate` DATETIME NULL,
  `UserId` BIGINT NOT NULL,
  PRIMARY KEY (`ShoppingCartID`),
  INDEX `fk_ShoppingCart_User_idx` (`UserId` ASC) VISIBLE,
  CONSTRAINT `fk_ShoppingCart_User`
    FOREIGN KEY (`UserId`)
    REFERENCES `mydb`.`User` (`UserId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`SupportTicket`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`SupportTicket` (
  `TicketID` BIGINT NOT NULL AUTO_INCREMENT,
  `IssueDescription` VARCHAR(200) NOT NULL,
  `CreationDate` DATETIME NOT NULL,
  `Status` VARCHAR(45) NOT NULL,
  `ResolutionDescription` VARCHAR(200) NULL,
  `UserId` BIGINT NOT NULL,
  PRIMARY KEY (`TicketID`),
  INDEX `fk_SupportTicket_User1_idx` (`UserId` ASC) VISIBLE,
  CONSTRAINT `fk_SupportTicket_User1`
    FOREIGN KEY (`UserId`)
    REFERENCES `mydb`.`User` (`UserId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`OrderProduct`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`OrderProduct` (
  `OrderID` BIGINT NOT NULL,
  `ProductId` BIGINT NOT NULL,
  `OrderProductId` BIGINT NOT NULL AUTO_INCREMENT,
  `Quantity` INT NOT NULL,
  `UnitPrice` DECIMAL NOT NULL,
  `LineTotal` DECIMAL NOT NULL,
  INDEX `fk_Order_has_Product_Product1_idx` (`ProductId` ASC) VISIBLE,
  INDEX `fk_Order_has_Product_Order1_idx` (`OrderID` ASC) VISIBLE,
  PRIMARY KEY (`OrderProductId`),
  CONSTRAINT `fk_Order_has_Product_Order1`
    FOREIGN KEY (`OrderID`)
    REFERENCES `mydb`.`Order` (`OrderID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Order_has_Product_Product1`
    FOREIGN KEY (`ProductId`)
    REFERENCES `mydb`.`Product` (`ProductId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`ShoppingCartProduct`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`ShoppingCartProduct` (
  `ShoppingCartID` BIGINT NOT NULL,
  `ProductId` BIGINT NOT NULL,
  `ShoppingCartProductId` BIGINT NOT NULL AUTO_INCREMENT,
  `Quantity` INT NOT NULL,
  INDEX `fk_ShoppingCart_has_Product_Product1_idx` (`ProductId` ASC) VISIBLE,
  INDEX `fk_ShoppingCart_has_Product_ShoppingCart1_idx` (`ShoppingCartID` ASC) VISIBLE,
  PRIMARY KEY (`ShoppingCartProductId`),
  CONSTRAINT `fk_ShoppingCart_has_Product_ShoppingCart1`
    FOREIGN KEY (`ShoppingCartID`)
    REFERENCES `mydb`.`ShoppingCart` (`ShoppingCartID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ShoppingCart_has_Product_Product1`
    FOREIGN KEY (`ProductId`)
    REFERENCES `mydb`.`Product` (`ProductId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
