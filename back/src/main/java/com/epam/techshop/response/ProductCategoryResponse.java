package com.epam.techshop.response;

import com.epam.techshop.domain.ProductCategory;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ProductCategoryResponse {
    private final List<ProductCategory> productCategory;
}
