package com.epam.techshop.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.catalina.LifecycleState;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationResponse {
    private String jwtToken;
    private User user;
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class User {
        private List<Role> role;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Role {
        private String roleName;
    }
}
