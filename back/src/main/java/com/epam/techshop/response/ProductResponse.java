package com.epam.techshop.response;

import com.epam.techshop.domain.Product;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ProductResponse {
    private final List<Product> products;
    private final PageMetadata page;
}