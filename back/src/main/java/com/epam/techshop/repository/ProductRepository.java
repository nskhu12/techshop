package com.epam.techshop.repository;

import com.epam.techshop.entity.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
    Page<ProductEntity> findAllByProductCategoryEntityProductCategoryId(Long categoryId, Pageable pageable);
    Page<ProductEntity> findAllByProductNameContaining(String name, Pageable pageable);
}
