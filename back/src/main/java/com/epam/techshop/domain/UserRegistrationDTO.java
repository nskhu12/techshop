package com.epam.techshop.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRegistrationDTO {
    private String userFirstName;
    private String userLastName;
    private String userName;
    private String userPassword;
}
