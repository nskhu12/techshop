package com.epam.techshop.service;

import com.epam.techshop.converter.ProductConverter;
import com.epam.techshop.converter.ProductDTOToEntityConverter;
import com.epam.techshop.domain.Product;
import com.epam.techshop.repository.ProductRepository;
import com.epam.techshop.response.PageMetadata;
import com.epam.techshop.response.ProductResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class ProductService {
    private final ProductRepository productRepository;
    private final ProductConverter productConverter;
    private final ProductDTOToEntityConverter productDTOToEntityConverter;

    public ProductService(ProductRepository productRepository, ProductConverter productConverter, ProductDTOToEntityConverter productDTOToEntityConverter) {
        this.productRepository = productRepository;
        this.productConverter = productConverter;
        this.productDTOToEntityConverter = productDTOToEntityConverter;
    }

    public ProductResponse getAllProductsByCategoryId(Long categoryId, Pageable pageable) {
        Page<Product> productPage = productRepository.findAllByProductCategoryEntityProductCategoryId(categoryId, pageable)
                .map(productConverter::convert);
        PageMetadata pageMetadata = new PageMetadata
                (productPage.getSize(), productPage.getTotalElements(), productPage.getTotalPages(), productPage.getNumber());
        return new ProductResponse(productPage.getContent(), pageMetadata);
    }

    public ProductResponse getAllProductsByProductName(String name, Pageable pageable) {
        Page<Product> productPage = productRepository.findAllByProductNameContaining(name, pageable).
                map(productConverter::convert);
        PageMetadata pageMetadata = new PageMetadata
                (productPage.getSize(), productPage.getTotalElements(), productPage.getTotalPages(), productPage.getNumber());
        return new ProductResponse(productPage.getContent(), pageMetadata);
    }

    public Product getProductById(Long id) {
        return productRepository.findById(id).map(productConverter::convert).orElse(null);
    }

    public boolean addProduct(Product productDTO) {
        try {
            productRepository.save(Objects.requireNonNull(productDTOToEntityConverter.convert(productDTO)));
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
