package com.epam.techshop.service;

import com.epam.techshop.converter.ProductCategoryConverter;
import com.epam.techshop.domain.ProductCategory;
import com.epam.techshop.entity.ProductCategoryEntity;
import com.epam.techshop.repository.ProductCategoryRepository;
import com.epam.techshop.response.ProductCategoryResponse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductCategoryService {
    private final ProductCategoryRepository productCategoryRepository;
    private final ProductCategoryConverter productCategoryConverter;

    public ProductCategoryService(ProductCategoryRepository productCategoryRepository, ProductCategoryConverter productCategoryConverter) {
        this.productCategoryRepository = productCategoryRepository;
        this.productCategoryConverter = productCategoryConverter;
    }

    public ProductCategoryResponse getAllCategories() {
        List<ProductCategory> categories = productCategoryRepository.findAll()
                .stream()
                .map(productCategoryConverter::convert)
                .collect(Collectors.toList());
        return new ProductCategoryResponse(categories);
    }

    public ProductCategory getCategoryById(Long categoryId) {
        Optional<ProductCategoryEntity> productCategoryEntityOptional = productCategoryRepository.findById(categoryId);
        return productCategoryEntityOptional.map(productCategoryConverter::convert).orElse(null);
    }
}
