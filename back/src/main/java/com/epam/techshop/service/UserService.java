package com.epam.techshop.service;

import com.epam.techshop.converter.UserRegistrationDTOConverter;
import com.epam.techshop.domain.UserRegistrationDTO;
import com.epam.techshop.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final UserRegistrationDTOConverter userRegistrationDTOConverter;
    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    public UserService(UserRepository userRepository, UserRegistrationDTOConverter userRegistrationDTOConverter) {
        this.userRepository = userRepository;
        this.userRegistrationDTOConverter = userRegistrationDTOConverter;
    }

    public boolean registerNewUser(UserRegistrationDTO userRegistrationDTO) {
        try {
            logger.info("Received registration request: {}", userRegistrationDTO);
            userRepository.save(Objects.requireNonNull(userRegistrationDTOConverter.convert(userRegistrationDTO)));
            logger.info("User registration successful for user: {}", userRegistrationDTO.getUserName());
            return true;
        } catch (Exception e) {
            logger.error("Error occurred during user registration", e);
            return false;
        }
    }
}