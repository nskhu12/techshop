package com.epam.techshop.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "Product")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ProductId")
    private Long productId;
    @Column(name = "SKU")
    private String sku;
    @Column(name = "ProductName")
    private String productName;
    @Column(name = "Description")
    private String description;
    @Column(name = "Price")
    private BigDecimal unitPrice;
    @Column(name = "ImageUrl")
    private String imageUrl;
    @Column(name = "Active")
    private boolean active;
    @Column(name = "StockQuantity")
    private int unitsInStock;
    @Column(name = "DateCreated")
    private LocalDate dateCreated;
    @Column(name = "LastUpdated")
    private LocalDate lastUpdated;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ProductCategoryId")
    private ProductCategoryEntity productCategoryEntity;
}
