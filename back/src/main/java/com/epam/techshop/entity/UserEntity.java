package com.epam.techshop.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "User")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "UserId")
    private Long userId;
    @Column(name = "FirstName")
    private String userFirstName;
    @Column(name = "LastName")
    private String userLastName;
    @Column(name = "UserName")
    private String userName;
    @Column(name = "Password")
    private String userPassword;
}
