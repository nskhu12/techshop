package com.epam.techshop.converter;

import com.epam.techshop.domain.Product;
import com.epam.techshop.entity.ProductEntity;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ProductConverter implements Converter<ProductEntity, Product> {
    private final ProductCategoryConverter productCategoryConverter;

    public ProductConverter(ProductCategoryConverter productCategoryConverter) {
        this.productCategoryConverter = productCategoryConverter;
    }

    @Override
    public Product convert(ProductEntity source) {
        return new Product(
                source.getProductId(), source.getSku(), source.getProductName(),
                source.getDescription(), source.getUnitPrice(), source.getImageUrl(),
                source.isActive(), source.getUnitsInStock(), source.getDateCreated(),
                source.getLastUpdated(), productCategoryConverter.convert(source.getProductCategoryEntity())
        );
    }
}
