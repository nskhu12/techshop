package com.epam.techshop.converter;

import com.epam.techshop.domain.Product;
import com.epam.techshop.entity.ProductEntity;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ProductDTOToEntityConverter implements Converter<Product, ProductEntity> {
    private final ProductCategoryDTOToEntityConverter productCategoryDTOToEntityConverter;

    public ProductDTOToEntityConverter(ProductCategoryDTOToEntityConverter productCategoryDTOToEntityConverter) {
        this.productCategoryDTOToEntityConverter = productCategoryDTOToEntityConverter;
    }

    @Override
    public ProductEntity convert(Product source) {
        return new ProductEntity(
                source.getId(), source.getSku(), source.getName(),
                source.getDescription(), source.getUnitPrice(), source.getImageUrl(),
                source.isActive(), source.getUnitsInStock(), source.getDateCreated(),
                source.getLastUpdated(), productCategoryDTOToEntityConverter.convert(source.getProductCategory())
        );
    }
}
