package com.epam.techshop.converter;

import com.epam.techshop.domain.UserRegistrationDTO;
import com.epam.techshop.entity.UserEntity;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserRegistrationDTOConverter implements Converter<UserRegistrationDTO, UserEntity> {
    @Override
    public UserEntity convert(UserRegistrationDTO source) {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName(source.getUserName());
        userEntity.setUserFirstName(source.getUserFirstName());
        userEntity.setUserLastName(source.getUserLastName());
        userEntity.setUserPassword(source.getUserPassword());
        return userEntity;
    }
}
