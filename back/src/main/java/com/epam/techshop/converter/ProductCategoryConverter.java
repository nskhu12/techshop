package com.epam.techshop.converter;

import com.epam.techshop.domain.ProductCategory;
import com.epam.techshop.entity.ProductCategoryEntity;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ProductCategoryConverter implements Converter<ProductCategoryEntity, ProductCategory> {
    @Override
    public ProductCategory convert(ProductCategoryEntity source) {
        return new ProductCategory(source.getProductCategoryId(), source.getProductCategoryName());
    }
}
