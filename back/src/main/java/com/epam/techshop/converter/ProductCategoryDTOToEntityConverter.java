package com.epam.techshop.converter;

import com.epam.techshop.domain.ProductCategory;
import com.epam.techshop.entity.ProductCategoryEntity;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ProductCategoryDTOToEntityConverter implements Converter<ProductCategory, ProductCategoryEntity> {

    @Override
    public ProductCategoryEntity convert(ProductCategory source) {
        return new ProductCategoryEntity(source.getId(), source.getCategoryName());
    }
}
