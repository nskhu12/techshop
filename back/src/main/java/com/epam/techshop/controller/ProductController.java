package com.epam.techshop.controller;

import com.epam.techshop.domain.Product;
import com.epam.techshop.response.ProductResponse;
import com.epam.techshop.service.ProductService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(value = "/api/products/search/findByCategoryId")
    public ResponseEntity<ProductResponse> getAllProductsByCategoryId(
            @RequestParam Long id,
            @PageableDefault(page = 0, size = 5) Pageable pageable
    ) {
        ProductResponse result = productService.getAllProductsByCategoryId(id, pageable);
        return ResponseEntity.ok(result);
    }

    @GetMapping(value = "/api/products/search/findByNameContaining")
    public ResponseEntity<ProductResponse> getAllProductsByName(
            @RequestParam String name,
            @PageableDefault(page = 0, size = 5) Pageable pageable
    ) {
        ProductResponse result = productService.getAllProductsByProductName(name, pageable);
        return ResponseEntity.ok(result);
    }

    @GetMapping(value = "/api/products/{productId}")
    public Product getProductById(@PathVariable("productId") Long id) {
        return productService.getProductById(id);
    }

    @PostMapping(value = "/api/product/add")
    public ResponseEntity<String> addProduct(@RequestBody Product productDTO) {
        return productService.addProduct(productDTO)
                ? ResponseEntity.status(HttpStatus.CREATED).body("Product added successfully.")
                : ResponseEntity.badRequest().body("Failed to add product.");
    }
}
