package com.epam.techshop.controller;

import com.epam.techshop.response.ProductCategoryResponse;
import com.epam.techshop.service.ProductCategoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductCategoryController {
    private final ProductCategoryService productCategoryService;

    public ProductCategoryController(ProductCategoryService productCategoryService) {
        this.productCategoryService = productCategoryService;
    }

    @GetMapping(value = "/api/product-category")
    public ResponseEntity<ProductCategoryResponse> getAllProductCategories() {
        ProductCategoryResponse result = productCategoryService.getAllCategories();
        return ResponseEntity.ok(result);
    }
}
