package com.epam.techshop.controller;

import com.epam.techshop.domain.UserAuthenticationDTO;
import com.epam.techshop.domain.UserRegistrationDTO;
import com.epam.techshop.response.AuthenticationResponse;
import com.epam.techshop.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

@RestController
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/registerNewUser")
    public ResponseEntity<String> registerNewUser(@RequestBody UserRegistrationDTO userRegistrationDTO) {
        return userService.registerNewUser(userRegistrationDTO)
                ? ResponseEntity.status(HttpStatus.CREATED).body("User registered successfully.")
                : ResponseEntity.badRequest().body("User registration failed.");
    }

    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponse> authenticate(@RequestBody UserAuthenticationDTO userAuthenticationDTO) {
        return ResponseEntity.ok().body(new AuthenticationResponse("testt", new AuthenticationResponse.User(Collections.singletonList(new AuthenticationResponse.Role("Admin")))));
    }
}
